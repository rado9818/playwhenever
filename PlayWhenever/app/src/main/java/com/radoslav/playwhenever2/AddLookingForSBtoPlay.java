package com.radoslav.playwhenever2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddLookingForSBtoPlay extends AppCompatActivity {

    private ArrayList<String> playgrounds = new ArrayList<>();
    TimePicker start, end;
    private TextView output;
    private Button changeDate;
    Float startTime;
    Float endTime;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;

    AutoCompleteTextView playgroundsAutoText;
    EditText gameType;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_looking_for_sbto_play);

        playgroundsAutoText = (AutoCompleteTextView)
                findViewById(R.id.playgroundsAutoText);
        gameType = (EditText) findViewById(R.id.gameType);
        submit = (Button) findViewById(R.id.submit);

        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds");
// Attach an listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
                                      @Override
                                      public void onDataChange(DataSnapshot snapshot) {
                                          playgrounds.clear();
                                          for (DataSnapshot child : snapshot.getChildren()) {
                                              System.out.println(child.getKey());
                                              playgrounds.add(child.getKey());

                                          }
                                      }

                                      @Override
                                      public void onCancelled(FirebaseError firebaseError) {
                                          System.out.println("The read failed: " + firebaseError.getMessage());
                                      }

                                  }
        );


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, playgrounds);

        playgroundsAutoText.setAdapter(adapter);






        start = (TimePicker) findViewById(R.id.start);
        end = (TimePicker) findViewById(R.id.end);

        start.setIs24HourView(true);
        end.setIs24HourView(true);

        output = (TextView) findViewById(R.id.outputDate);
        changeDate = (Button) findViewById(R.id.setDate);

        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);

        output.setText(new StringBuilder()
                .append(day).append("/").append(month + 1).append("/")
                .append(year));

        changeDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DATE_PICKER_ID);

            }

        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playgroundsAutoText.setError(null);
                startTime = timeToFloat(start.getCurrentHour() + ":" + start.getCurrentMinute());
                endTime = timeToFloat(end.getCurrentHour() + ":" + end.getCurrentMinute());
                if (playgrounds.indexOf(playgroundsAutoText.getText().toString()) != -1){
                    if(!hasTheAppointmentPassed()) {
                        if(!gameType.getText().toString().equals("")) {
                            add();
                            Toast.makeText(getApplicationContext(), getString(R.string.added), Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            gameType.setError("This field cannot be empty");
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Date or time not valid!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    playgroundsAutoText.setError("Enter an existing playground!");
                }
            }
        });


    }



    private void add(){
        Firebase ref = new Firebase(Constants.firebaseUrl).child("wantToPlay/" + UserDetails.userName+"/"+playgroundsAutoText.getText().toString());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Firebase ref = new Firebase(Constants.firebaseUrl).child("wantToPlay/" + UserDetails.userName+"/"+playgroundsAutoText.getText().toString());

                Map<String, Object> update = new HashMap<>();
                update.put("playground", playgroundsAutoText.getText().toString());
                update.put("gameType", gameType.getText().toString());
                update.put("date", output.getText().toString());
                update.put("startTime", start.getCurrentHour() + ":" + start.getCurrentMinute());
                update.put("endTime", end.getCurrentHour() + ":" + end.getCurrentMinute());
                update.put("user", UserDetails.userName);
                ref.updateChildren(update);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        Toast.makeText(getApplicationContext(), R.string.appointment_added, Toast.LENGTH_LONG).show();

    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                return new DatePickerDialog(this, pickerListener, year, month,day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            output.setText(new StringBuilder().append(day)
                    .append("/").append(month + 1).append("/").append(year));
        }

    };




    private Boolean hasTheAppointmentPassed()
    {
        final Calendar c = Calendar.getInstance();

        System.out.print("c.get(Calendar.DAY_OF_MONTH) " + c.get(Calendar.DAY_OF_MONTH) + "\n");
        System.out.print("day " + day + "\n");

        int monthToday = c.get(Calendar.MONTH)+1;
        int monthChosen = month++;

        System.out.print("monthToday " + monthToday + "\n");
        System.out.print("month " + monthChosen + "\n");

        System.out.print("c.get(Calendar.YEAR) " + c.get(Calendar.YEAR) + "\n");
        System.out.print("year) " + year + "\n");

        Float startToCompare = Float.parseFloat(startTime-1 + "");

        Float currentTime = timeToFloat(c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));

        System.out.print("startToCompare " + startToCompare + "\n");
        System.out.print("current time " + currentTime + "\n");

        System.out.print("\n dates ");
        System.out.print(c.get(Calendar.DAY_OF_MONTH) == day);
        System.out.print("\n months ");
        System.out.print( monthToday <= month);
        System.out.print("\n years ");
        System.out.print(c.get(Calendar.YEAR) <= year);
        System.out.print("\n hours ");
        System.out.print(startToCompare > currentTime);
        System.out.print("\n");

        if(c.get(Calendar.DAY_OF_MONTH) < day   &&     monthToday <= monthChosen    &&     c.get(Calendar.YEAR) <= year)
        {
            System.out.println("false");
            return  false;

        }
        else if (c.get(Calendar.DAY_OF_MONTH) == day   &&     monthToday <= month    &&     c.get(Calendar.YEAR) <= year    && startToCompare > currentTime)
        {
            System.out.println("false");
            return false;
        }
        System.out.println("true");
        return true;

    }




    private Float timeToFloat(String time)
    {
        float timeFloat = Float.parseFloat(time.replace(":", "."));
        Float resultTo100 = convertTo100(timeFloat);
        System.out.println(resultTo100);
        return resultTo100;
    }
    private static Float convertTo100(float input)
    {
        String input_string = Float.toString(input);
        BigDecimal inputBD = new BigDecimal(input_string);
        String hhStr = input_string.split("\\.")[0];
        BigDecimal output = new BigDecimal(Float.toString(Integer.parseInt(hhStr)));
        output = output.add((inputBD.subtract(output).divide(BigDecimal.valueOf(60), 10, BigDecimal.ROUND_HALF_EVEN)).multiply(BigDecimal.valueOf(100)));

        return Float.parseFloat(output.toString());
    }
}
