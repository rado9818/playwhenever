package com.radoslav.playwhenever2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;

public class SearchPlaygrounds extends Activity implements OnItemClickListener {


    private EditText inputSearch;




    ArrayAdapter<String> adapter;



    private ArrayList<String> playgrounds;
    private ArrayList<TextDrawable> images = new ArrayList<>();


    private ListView lv;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_playgrounds);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        playgrounds = new ArrayList<>(intent.getStringArrayListExtra("playgrounds"));



        String[] playgroundsArray = playgrounds.toArray(new String[playgrounds.size()]);
        for(int i=0; i<playgroundsArray.length; i++){
            System.out.println("play at "+i + " "+ playgroundsArray[i]);
        }

        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);

        adapter = new ArrayAdapter<>(this, R.layout.simple_list_item, R.id.name, playgroundsArray);
        lv.setAdapter(adapter);


        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                SearchPlaygrounds.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String playgroundName = ((TextView) view.findViewById(R.id.name)).getText()
                        .toString();

                System.out.println(playgroundName);
                Intent intent = new Intent(SearchPlaygrounds.this, PlaygroundAppointments.class);
                intent.putExtra("playground", playgroundName);
                startActivity(intent);


            }
        });
    }



    @Override
    protected void onRestart()
    {
        super.onRestart();

        recreate();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }



}