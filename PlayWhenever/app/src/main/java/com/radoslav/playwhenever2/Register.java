package com.radoslav.playwhenever2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class Register extends AppCompatActivity {

    EditText emailText, passwordText;
    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        emailText = (EditText) findViewById(R.id.email);
        passwordText = (EditText) findViewById(R.id.password);
        register = (Button) findViewById(R.id.registerButton);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hash hash = null;
                try {
                    hash = new Hash(passwordText.getText().toString());
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                String password = hash.getHashedValue();

                createAccount(emailText.getText().toString(), password);
            }
        });

    }

    private void createAccount(String email, String password)
    {
        Firebase ref = new Firebase(Constants.firebaseUrl);
        ref.createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> result) {
                System.out.println("Successfully created user account with uid: " + result.get("uid"));
            }

            @Override
            public void onError(FirebaseError firebaseError) {

System.out.print(firebaseError.getMessage()
);            }
        });
    }
}
