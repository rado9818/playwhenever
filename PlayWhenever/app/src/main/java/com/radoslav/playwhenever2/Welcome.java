package com.radoslav.playwhenever2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2Fragment;

public class Welcome extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();


        addSlide(AppIntro2Fragment.newInstance(getString(R.string.welcome_first), getString(R.string.moto),
                R.drawable.launcher, getResources().getColor(R.color.blue)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.intro1), getString(R.string.key1),
                R.drawable.launcher, getResources().getColor(R.color.blue)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.they_will_as_well), getString(R.string.key2),
                R.drawable.launcher, getResources().getColor(R.color.blue)));
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.dont_wait), getString(R.string.key3),
                R.drawable.launcher, getResources().getColor(R.color.blue)));


        setBarColor(getResources().getColor(R.color.blue));
        setSeparatorColor(getResources().getColor(R.color.blue));

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
