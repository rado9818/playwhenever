package com.radoslav.playwhenever2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ReportSamePlayground extends AppCompatActivity {
    ListView lv;
    TextView whySame;

    private ArrayList<String> playgrounds = new ArrayList<>();

    private ArrayList<String> times = new ArrayList<>();

    private ArrayList<String> hoursFree = new ArrayList<>();
    private ArrayList<TextDrawable> images = new ArrayList<>();

    private Firebase timeReference;

    private String playgroundToReport;


    private final static String DASH = " - ";
    private final static String SLASH = "/";
    private final static String COLON = ":";
    private final static int SUCCESS = 1;
    private final static int FAILED = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_same_playground);


        lv = (ListView) findViewById(R.id.listView);
        whySame = (TextView) findViewById(R.id.playgrounds_identical_txt);
        Intent intent = getIntent();
        playgroundToReport = intent.getStringExtra("playground");

        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds");
// Attach an listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
                                      @Override
                                      public void onDataChange(DataSnapshot snapshot) {
                                          playgrounds.clear();
                                          for (DataSnapshot child : snapshot.getChildren()) {
                                              if(!child.getKey().equals(playgroundToReport)) {
                                                System.out.println(child.getKey());
                                                playgrounds.add(child.getKey());
                                                getTimes(child.getKey());


                                                String letter = "A";

                                                letter = String.valueOf((child.getKey()).charAt(0)).toUpperCase();


                                                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
                                                int color = generator.getRandomColor();
                                                TextDrawable drawable = TextDrawable.builder()
                                                      .buildRoundRect(letter, color, 20);
                                                images.add(drawable);

                                          }

                                        }
                                      }

                                      @Override
                                      public void onCancelled(FirebaseError firebaseError) {
                                          System.out.println("The read failed: " + firebaseError.getMessage());
                                      }


                                  }
        );







        try {
            lv.setAdapter(new VersionAdapter(ReportSamePlayground.this));
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                        long arg3) {
                    whySame.setError(null);
                    if(noMoreThanHundredChars()) {
                        String groupToApplyName = ((TextView) arg1.findViewById(R.id.name)).getText()
                                .toString();
                        System.out.println(groupToApplyName);

                        int compare = groupToApplyName.compareTo(playgroundToReport);
                        if (compare < 0) {
                            System.out.println(groupToApplyName + " is before " + playgroundToReport);
                            sendReport(groupToApplyName + DASH + playgroundToReport, groupToApplyName);
                        } else if (compare > 0) {
                            System.out.println(playgroundToReport + " is before " + groupToApplyName);
                            sendReport(playgroundToReport + DASH + groupToApplyName, groupToApplyName);

                        } else {
                            System.out.println(playgroundToReport + " is same as " + groupToApplyName);
                            sendReport(playgroundToReport + DASH + groupToApplyName, groupToApplyName);
                        }


                        // ImageView iv = (ImageView) layout.findViewById(R.id.toast_iv);

                        // iv.setBackgroundResource(thumb[pos]); //****************************
                    }
                    else
                    {
                        whySame.setError(getString(R.string.no_more_than_two_hundred_chars));
                    }
                }
            });

        }
        catch (Exception e)
        {
            recreate();
            Toast.makeText(getApplicationContext(), getString(R.string.handled_unexpected), Toast.LENGTH_LONG).show();
        }


    }

    private void sendReport(final String report, final String second){
        Calendar c = Calendar.getInstance();
        final Firebase ref = new Firebase(Constants.firebaseUrl).child("reports/" + ((c.get(Calendar.MONTH))+1)+c.get(Calendar.MINUTE)+c.get(Calendar.MILLISECOND));
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Calendar c = Calendar.getInstance();
                Map<String, Object> update = new HashMap<>();
                update.put("report", report);
                update.put("first playground clicked", playgroundToReport);
                update.put("playground clicked later (second)", second);
                update.put("user", UserDetails.userName);
                update.put("date", c.get(Calendar.DAY_OF_MONTH) + SLASH + ((c.get(Calendar.MONTH)) + 1) + SLASH + c.get(Calendar.YEAR));
                update.put("hours", c.get(Calendar.HOUR_OF_DAY) + COLON + c.get(Calendar.MINUTE) + COLON + c.get(Calendar.SECOND));
                update.put("time zone", getTimeZoneByLocale());
                if (!whySame.getText().toString().equals("")) {
                    update.put("reasons", whySame.getText().toString());
                } else {
                    update.put("reasons", "Not entered by the user!");
                }
                ref.updateChildren(update);
                Toast.makeText(getApplicationContext(), R.string.report_sent_successfully,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getApplicationContext(), R.string.report_not_successful,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean noMoreThanHundredChars()
    {
        return whySame.getText().length() <= 200;
    }

    private String getTimeZoneByLocale(){
        System.out.println(TimeZone.getDefault().getDisplayName());

        return TimeZone.getDefault().getDisplayName();
    }


    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(ReportSamePlayground activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return playgrounds.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item, null);
            }

            // Initialize the views in the layout
            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvTitle = (TextView) listItem.findViewById(R.id.name);
            TextView tvFree = (TextView) listItem.findViewById(R.id.free);
            String letter = "A";

            letter = String.valueOf(playgrounds.get(pos).charAt(0)).toUpperCase();


            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRoundRect(letter, color, 20);



            //  iv.setBackgroundResource(thumb[pos]);  // **************
            try {
                tvTitle.setText(playgrounds.get(pos));
                System.out.println("title " + playgrounds.get(pos));
                tvFree.setText(hoursFree.get(pos));
                System.out.println("hours " + hoursFree.get(pos));

                iv.setImageDrawable(drawable);
            }
            catch (Exception e)
            {
                recreate();
                System.out.println("Exc: " + e);
                Toast.makeText(getApplicationContext(), getString(R.string.handled_unexpected), Toast.LENGTH_LONG).show();
            }

            return listItem;
        }

    }





    private void getTimes(String playground)
    {
        timeReference = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground);

        timeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                times.clear();
                int names = 0;

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    System.out.print(child.getKey() + " " + child.getChildrenCount());

                    if(!child.getKey().equals("location") && (!child.getKey().equals("locale")))
                    {
                        System.out.print(child.getKey());
                        GetPlaygroundCharacteristics get = child.getValue(GetPlaygroundCharacteristics.class);

                        times.add(get.getStartTime());
                        times.add(get.getEndTime());

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        Date tomorrow = calendar.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
                        String tomorrowAsString = dateFormat.format(tomorrow);

                        System.out.println(tomorrowAsString);

                        if(get.getDate().equals(tomorrowAsString)) {
                            System.out.print("date: " + get.getDate() + "\n");
                            System.out.print("end: " + get.getEndTime() + "\n");
                            System.out.print("start: " + get.getStartTime() + "\n");
                            FreeHoursGenerator freeHoursGenerator = new FreeHoursGenerator(times);

                            hoursFree.add(freeHoursGenerator.getHoursFree());
                            names += 1;
                        }
                    }

                }


                if(names == 0)
                {
                    System.out.println("this happens");
                    hoursFree.add("always");
                }
                names = 0;



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

}
