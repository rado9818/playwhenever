package com.radoslav.playwhenever2;

import java.util.ArrayList;

/**
 * Created by Rado on 19/04/2016.
 */
public class FreeHoursGenerator {
    private String hoursFree = "";
    private final String TO = " to ";
    public FreeHoursGenerator(ArrayList<String> busyHours)
    {
        for (int i=0; i<busyHours.size();i++){
            System.out.println(busyHours.get(i) + "\n");
    }
        if(busyHours.get(0) != null) {
            if(busyHours.get(0) != "00:00") {

                hoursFree += "00:00" + TO + busyHours.get(0) + "; ";

                for (int i = 1; i < busyHours.size(); i++) {
                    if (i % 2 != 0) {
                        hoursFree += busyHours.get(i);
                        if(i == busyHours.size()-1)
                        {
                            hoursFree += TO + "24:00 ";
                        }
                    } else {
                        hoursFree += TO + busyHours.get(i) + "; ";
                    }
                }
            }
        }
        else
        {
            hoursFree = "always";
        }
        System.out.println("resulttttt " + hoursFree);

    }


   public String getHoursFree()
   {
       return hoursFree;
   }
}
