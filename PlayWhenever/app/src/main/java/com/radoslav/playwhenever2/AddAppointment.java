package com.radoslav.playwhenever2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class AddAppointment extends AppCompatActivity {

    TimePicker start;
    Button submit;
    private String playground;
    private String locale;

    private TextView output;
    private Button changeDate;
    Float startTime;
    Float endTime;
    private int year;
    private int month;
    private int day;
    static final int DATE_PICKER_ID = 1111;

    Switch mySwitch;

    private int startHour;
    private int startMinutes;

    private int endHour;
    private int endMinutes;

    private ArrayList<Float> hoursBusy = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);
        Intent intent = getIntent();
         playground = intent.getStringExtra("playground");
       locale = intent.getStringExtra("locale");

        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);

        startHour = c.get(Calendar.HOUR_OF_DAY);
        startMinutes = c.get(Calendar.MINUTE);
        endHour = c.get(Calendar.HOUR_OF_DAY)+1;
        endMinutes = c.get(Calendar.MINUTE)+1;

        start = (TimePicker) findViewById(R.id.start);
        start.setIs24HourView(true);

        mySwitch = (Switch) findViewById(R.id.mySwitch);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("changeee " + buttonView.isChecked());
                if(!buttonView.isChecked())
                {
                    start.setCurrentHour(startHour);
                    start.setCurrentMinute(startMinutes);
                }
                else
                {
                    start.setCurrentHour(endHour);
                    start.setCurrentMinute(endMinutes);
                }
            }
        });


        start.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {

                if(!mySwitch.isChecked())
                {
                    startHour = hour;
                    startMinutes = minute;
                }
                else
                {
                    endHour = hour;
                    endMinutes = minute;
                }

                System.out.println(startHour + " " + startMinutes + " " + endHour + " " + endMinutes);
            }
        });



        output = (TextView) findViewById(R.id.outputDate);
        changeDate = (Button) findViewById(R.id.setDate);



        output.setText(new StringBuilder()
                .append(day).append("/").append(month + 1).append("/")
                .append(year));

        changeDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DATE_PICKER_ID);

            }

        });


        getTimeZoneByLocale();
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = timeToFloat(startHour + ":" + startMinutes);
                endTime = timeToFloat(endHour + ":" + endMinutes);
                System.out.print("start " + startTime + "\n");
                System.out.print("end " + endTime + "\n");

                for (int i = 0; i < hoursBusy.size(); i++) {
                    System.out.println("the array " + hoursBusy.get(i) + "\n");
                }

                Boolean result;
                try {
                    CheckAvailability checkAvailability = new CheckAvailability(hoursBusy, startTime, endTime);
                    result = checkAvailability.getResult();
                    System.out.println("result " + result);
                }
                catch (Exception e)
                {
                    result =false;
                }
                if (!result) {

                    if (!locale.equals(getTimeZoneByLocale())) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddAppointment.this);
                        dialogBuilder.setMessage(com.radoslav.playwhenever2.R.string.different_timzeone);
                        dialogBuilder.setCancelable(false);
                        dialogBuilder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if(endBigger()) {
                                            if((!hasTheAppointmentPassed()))
                                            {
                                                if(noMoreThanTwoHours()) {
                                                    add();
                                                }
                                                else
                                                {
                                                    Toast.makeText(getApplicationContext(), getString(R.string.max_2_hours), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                              else{
                                            Toast.makeText(getApplicationContext(), R.string.appointment_in_future_in_advance, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                        else
                                        {
                                            Toast.makeText(getApplicationContext(), R.string.end_time_bigger_at_least_30_minutes, Toast.LENGTH_LONG).show();
                                        }

                                        dialog.cancel();
                                    }
                                });
                        dialogBuilder.setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });


                        AlertDialog showTheAlert = dialogBuilder.create();
                        showTheAlert.show();

                    } else {
                        if(endBigger()) {
                            if((!hasTheAppointmentPassed()))
                            {
                                if(noMoreThanTwoHours()) {
                                    add();
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(), getString(R.string.max_2_hours), Toast.LENGTH_LONG).show();
                                }

                            }
                            else{
                                Toast.makeText(getApplicationContext(), R.string.appointment_in_future_in_advance, Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), R.string.end_time_bigger_at_least_30_minutes, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddAppointment.this);
                    dialogBuilder.setMessage("The time you want to appoint is already taken. Please change it.");
                    dialogBuilder.setCancelable(false);
                    dialogBuilder.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog showTheAlert = dialogBuilder.create();
                    showTheAlert.show();
                }
            }
        });
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                return new DatePickerDialog(this, pickerListener, year, month,day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            output.setText(new StringBuilder().append(day)
                    .append("/").append(month+1).append("/").append(year));
            updateHours();
        }

    };



    private void updateHours(){

        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                hoursBusy.clear();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    System.out.println(child.getKey());

                    if(!child.getKey().equals("location") && (!child.getKey().equals("locale")))
                    {
                        System.out.print(child.getKey());
                        GetPlaygroundCharacteristics get = child.getValue(GetPlaygroundCharacteristics.class);

                        System.out.println("get.getDate() " + get.getDate() + "\n");
                        System.out.println("output.getText().toString() " + output.getText().toString() + "\n");
                        if(get.getDate().equals(output.getText().toString())) {
                            hoursBusy.add(timeToFloat(get.getStartTime()));
                            hoursBusy.add(timeToFloat(get.getEndTime()));
                        }

                    }

                }



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }



    private void add(){
        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground + "/" + UserDetails.userName);
                Map<String, Object> update = new HashMap<>();
                update.put("date", output.getText().toString());
                update.put("startTime", startHour + ":" + startMinutes);
                update.put("endTime", endHour + ":" + endMinutes);
                ref.updateChildren(update);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        Toast.makeText(getApplicationContext(), R.string.appointment_added, Toast.LENGTH_LONG).show();

    }

    public String getTimeZoneByLocale(){
        System.out.println(TimeZone.getDefault().getDisplayName());

        return TimeZone.getDefault().getDisplayName();
    }

    private Float timeToFloat(String time)
    {
        float timeFloat = Float.parseFloat(time.replace(":", "."));
        Float resultTo100 = convertTo100(timeFloat);
        System.out.println(resultTo100);
        return resultTo100;
    }
    private static Float convertTo100(float input)
    {
        String input_string = Float.toString(input);
        BigDecimal inputBD = new BigDecimal(input_string);
        String hhStr = input_string.split("\\.")[0];
        BigDecimal output = new BigDecimal(Float.toString(Integer.parseInt(hhStr)));
        output = output.add((inputBD.subtract(output).divide(BigDecimal.valueOf(60), 10, BigDecimal.ROUND_HALF_EVEN)).multiply(BigDecimal.valueOf(100)));

        return Float.parseFloat(output.toString());
    }
    private Boolean hasTheAppointmentPassed()
    {
        final Calendar c = Calendar.getInstance();

        System.out.print("c.get(Calendar.DAY_OF_MONTH) " + c.get(Calendar.DAY_OF_MONTH) + "\n");
        System.out.print("day " + day + "\n");

        int monthToday = c.get(Calendar.MONTH)+1;
        int monthChosen = month++;

        System.out.print("monthToday " + monthToday + "\n");
        System.out.print("month " + monthChosen + "\n");

        System.out.print("c.get(Calendar.YEAR) " + c.get(Calendar.YEAR) + "\n");
        System.out.print("year) " + year + "\n");

        Float startToCompare = Float.parseFloat(startTime-1 + "");

        Float currentTime = timeToFloat(c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));

        System.out.print("startToCompare " + startToCompare + "\n");
        System.out.print("current time " + currentTime + "\n");

        System.out.print("\n dates ");
        System.out.print(c.get(Calendar.DAY_OF_MONTH) == day);
        System.out.print("\n months ");
        System.out.print( monthToday <= month);
        System.out.print("\n years ");
        System.out.print(c.get(Calendar.YEAR) <= year);
        System.out.print("\n hours ");
        System.out.print(startToCompare > currentTime);
        System.out.print("\n");

        if(c.get(Calendar.DAY_OF_MONTH) < day   &&     monthToday <= monthChosen    &&     c.get(Calendar.YEAR) <= year)
        {
            System.out.println("false");
            return  false;

        }
        else if (c.get(Calendar.DAY_OF_MONTH) == day   &&     monthToday <= month    &&     c.get(Calendar.YEAR) <= year    && startToCompare > currentTime)
        {
            System.out.println("false");
            return false;
        }
        System.out.println("true");
        return true;

    }


    private Boolean endBigger()
    {

        Float startToCompare = Float.parseFloat(startTime+0.5 + "");
        System.out.println("\n" + "startToCompare " + startToCompare +"\n");
        System.out.println("\n" + "endTime " + endTime +"\n");
        return startToCompare<endTime;

    }
    private Boolean noMoreThanTwoHours()
    {
        return endTime-startTime < 2;
    }
}
