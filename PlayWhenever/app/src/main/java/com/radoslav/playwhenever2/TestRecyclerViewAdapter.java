package com.radoslav.playwhenever2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;
import java.util.List;


public class TestRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Object> contents;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;
    private ArrayList<String> playgroundsToPass, datesToPass;
    private ArrayList<TextDrawable> drawablesToPass;
    private static int itemCount;
    int count = 0;

    public TestRecyclerViewAdapter(List<Object> contents, int number, ArrayList<String> playgrounds, ArrayList<String> dates, ArrayList<TextDrawable> drawables) {
        this.contents = contents;
        itemCount = number;
        playgroundsToPass = new ArrayList<>(playgrounds);
        datesToPass = new ArrayList<>(dates);
        drawablesToPass = new ArrayList<>(drawables);
        for(int i=0; i<playgroundsToPass.size(); i++)
        {
            System.out.println(i);
            System.out.println("\n" + playgroundsToPass.get(i));
            System.out.println("\n" + datesToPass.get(i));
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case TYPE_HEADER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_big, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }
            case TYPE_CELL: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_small, parent, false);
                TextView playground = (TextView) view.findViewById(R.id.place);
                TextView date = (TextView) view.findViewById(R.id.date);
                ImageView letter = (ImageView) view.findViewById(R.id.letter);
                playground.setText(playgroundsToPass.get(count));
                date.setText(datesToPass.get(count));
                letter.setImageDrawable(drawablesToPass.get(count));
                System.out.println(drawablesToPass.get(count));
                count++;

                return new RecyclerView.ViewHolder(view) {
                };
            }
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                break;
            case TYPE_CELL:
                break;
        }
    }
}