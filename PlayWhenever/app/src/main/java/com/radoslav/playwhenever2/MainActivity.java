package com.radoslav.playwhenever2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends DrawerActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Button add;
    ListView lv;
    private ArrayList<String> playgrounds = new ArrayList<>();
    private ArrayList<String> wantToPlay = new ArrayList<>();

    private ArrayList<String> times = new ArrayList<>();

    private ArrayList<String> hoursFree = new ArrayList<>();
    private ArrayList<TextDrawable> images = new ArrayList<>();

    private Firebase timeReference;

    GoogleApiClient mGoogleApiClient;

    final String PREFS_NAME = "MainActivity";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    private MaterialViewPager mViewPager;
    private Toolbar toolbar;
    ImageView fab;
    Button searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        FacebookSdk.sdkInitialize(getApplicationContext());


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        if (!UserDetails.loggedIn) {
            Intent intent = new Intent(this, LogIn.class);
            startActivityForResult(intent, 66);
        }

        lv = (ListView) findViewById(R.id.listView);
        fab = (ImageView) findViewById(R.id.fab);



        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds");
// Attach an listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
                                      @Override
                                      public void onDataChange(DataSnapshot snapshot) {
                                          playgrounds.clear();
                                          for (DataSnapshot child : snapshot.getChildren()) {
                                              System.out.println(child.getKey());
                                              playgrounds.add(child.getKey());
                                              getTimes(child.getKey());


                                              String letter = "A";

                                              letter = String.valueOf((child.getKey()).charAt(0)).toUpperCase();


                                              ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
                                              int color = generator.getRandomColor();
                                              TextDrawable drawable = TextDrawable.builder()
                                                      .buildRoundRect(letter, color, 20);
                                              images.add(drawable);

                                          }


                                      }

                                      @Override
                                      public void onCancelled(FirebaseError firebaseError) {
                                          System.out.println("The read failed: " + firebaseError.getMessage());
                                      }

                                  }
        );


        fab.bringToFront();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddPlayground.class);
                startActivity(intent);
            }
        });

        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
        if (savedVersionCode == DOESNT_EXIST) {
            startActivity(new Intent(this, Welcome.class));
        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();


        setTitle("");

        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);

        toolbar = mViewPager.getToolbar();
        System.out.println(toolbar.getTitle());


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            System.out.println("NOT NULLLLLLL");

        }

        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position % 4) {
                     case 0:
                        return RecyclerViewFragment.newInstance(playgrounds.size() + 1, playgrounds, hoursFree, images);

                    default:
                        return RecyclerViewFragment.newInstance(playgrounds.size() + 1, playgrounds, hoursFree, images);
                }
            }

            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position % 4) {
                    case 0:
                        return "Playgrounds";
                }
                return "";
            }
        });
        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 0:
                        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

                            @Override
                            public int getCount() {
                                return 1;
                            }

                            @Override
                            public Fragment getItem(int position) {
                                return RecyclerViewFragment.newInstance(playgrounds.size() + 1, playgrounds, hoursFree, images);
                            }
                        });
                        return HeaderDesign.fromColorAndDrawable(
                                getResources().getColor(R.color.green),
                                getResources().getDrawable(R.drawable.header));
                    case 1:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.blue,
                                "http://cdn1.tnwcdn.com/wp-content/blogs.dir/1/files/2014/06/wallpaper_51.jpg");
                    case 2:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.cyan,
                                "http://www.droid-life.com/wp-content/uploads/2014/10/lollipop-wallpapers10.jpg");
                    case 3:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.red,
                                "http://www.tothemobile.com/wp-content/uploads/2014/07/original.jpg");
                }

                //execute others actions if needed (ex : modify your header logo)

                return null;
            }
        });


        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());



    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //recreate();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.lookAndPlay:
                Intent intent = new Intent(this, LookForSBtoPlay.class);
                lookForSBtoPlay();
                for(int i=0; i<playgrounds.size(); i++)
                {
                    System.out.println("prior "+playgrounds.get(i));
                }
                String[] playgroundsArray = playgrounds.toArray(new String[playgrounds.size()]);
                intent.putExtra("wantToPlay", playgroundsArray);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void lookForSBtoPlay(){
        Firebase ref = new Firebase(Constants.firebaseUrl).child("wantToPlay");
// Attach an listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
                                      @Override
                                      public void onDataChange(DataSnapshot snapshot) {
                                          wantToPlay.clear();
                                          for (DataSnapshot child : snapshot.getChildren()) {
                                              wantToPlay.add(child.getKey());

                                          }


                                      }

                                      @Override
                                      public void onCancelled(FirebaseError firebaseError) {
                                          System.out.println("The read failed: " + firebaseError.getMessage());
                                      }

                                  }
        );
    }

    public void cardClicked(View v) {
        TextView playground = (TextView) v.findViewById(R.id.place);
        System.out.println(playground.getText().toString());

        Intent intent = new Intent(MainActivity.this, PlaygroundAppointments.class);
        intent.putExtra("playground", playground.getText());
        startActivity(intent);
    }

    public void searchClicked (View view){
        Intent intent = new Intent(this, SearchPlaygrounds.class);
        intent.putExtra("playgrounds", playgrounds);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 66 && resultCode == Constants.EXIT_REQUEST_FROM_LOGIN) {
            signOutFromGplus();
            finish();
        }
    }

    private void getTimes(String playground) {
        timeReference = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground);

        timeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                times.clear();
                int names = 0;

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    System.out.print(child.getKey() + " " + child.getChildrenCount());

                    if (!child.getKey().equals("location") && (!child.getKey().equals("locale"))) {
                        System.out.print(child.getKey());
                        GetPlaygroundCharacteristics get = child.getValue(GetPlaygroundCharacteristics.class);

                        times.add(get.getStartTime());
                        times.add(get.getEndTime());

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        Date tomorrow = calendar.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
                        String tomorrowAsString = dateFormat.format(tomorrow);

                        System.out.println(tomorrowAsString);

                        if (get.getDate().equals(tomorrowAsString)) {
                            System.out.print("date: " + get.getDate() + "\n");
                            System.out.print("end: " + get.getEndTime() + "\n");
                            System.out.print("start: " + get.getStartTime() + "\n");
                            FreeHoursGenerator freeHoursGenerator = new FreeHoursGenerator(times);

                            hoursFree.add(freeHoursGenerator.getHoursFree());
                            names += 1;
                        }
                    }

                }


                if (names == 0) {
                    System.out.println("this happens");
                    hoursFree.add("always");
                }
                names = 0;


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        signOutFromGplus();
        signOutFromFB();


    }

    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    private void signOutFromFB() {
        LoginManager.getInstance().logOut();
    }

}