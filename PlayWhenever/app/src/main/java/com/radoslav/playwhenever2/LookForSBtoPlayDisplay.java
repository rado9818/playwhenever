package com.radoslav.playwhenever2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

public class LookForSBtoPlayDisplay extends AppCompatActivity {
    private ArrayList<String> gameType = new ArrayList<>();
    private ArrayList<TextDrawable> images = new ArrayList<>();
    private ArrayList<String> date = new ArrayList<>();
    private ArrayList<String> startTime = new ArrayList<>();
    private ArrayList<String> endTime = new ArrayList<>();
    private ArrayList<String> user = new ArrayList<>();
    String playgroundName;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_for_sbto_play_display);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        lv = (ListView) findViewById(R.id.listView);


        try {
            lv.setAdapter(new VersionAdapter(LookForSBtoPlayDisplay.this));
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        }
        catch (Exception e)
        {
            recreate();
            Toast.makeText(getApplicationContext(), getString(R.string.handled_unexpected), Toast.LENGTH_LONG).show();
        }









        playgroundName = getIntent().getStringExtra("playground");
        Firebase ref = new Firebase(Constants.firebaseUrl+"/wantToPlay/");
// Attach an listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                gameType.clear();
                for (DataSnapshot child : snapshot.getChildren()) {
                    System.out.println(child.getKey());
                    getData(child.getKey());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        });

        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), AddLookingForSBtoPlay.class);
                    startActivity(intent);
                }
            });
        }

    }




private void getData(String userName)
{
    Firebase ref = new Firebase(Constants.firebaseUrl+"/wantToPlay/"+userName+"/"+playgroundName);
    System.out.println(Constants.firebaseUrl+"/wantToPlay/"+userName+playgroundName);
// Attach an listener to read the data at our posts reference
    ref.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            gameType.clear();
            for (DataSnapshot child : snapshot.getChildren()) {
                System.out.println(child.getKey());
                    if(child.getValue().equals(playgroundName)){
                    gameType.add(String.valueOf(child.getValue()));
                    System.out.println("game type" + child.getValue());

                    String letter = "A";
                    letter = String.valueOf((child.getKey()).charAt(0)).toUpperCase();


                    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
                    int color = generator.getRandomColor();
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRoundRect(letter, color, 20);
                    images.add(drawable);
                } else if (child.getKey().equals("date")) {
                    date.add(String.valueOf(child.getValue()));
                } else if (child.getKey().equals("startTime")) {
                    startTime.add(String.valueOf(child.getValue()));
                } else if (child.getKey().equals("endTime")) {
                    endTime.add(String.valueOf(child.getValue()));

                }
                else if (child.getKey().equals("user")) {
                        user.add(String.valueOf(child.getValue()));
                    }

            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }

    });
    System.out.println("data to display dor looking for sb to play");


}


    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(LookForSBtoPlayDisplay activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return gameType.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item_play_with_sb, null);
            }

            // Initialize the views in the layout
            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvGameType = (TextView) listItem.findViewById(R.id.game_type);
            TextView tvDate = (TextView) listItem.findViewById(R.id.date_tv);
            TextView tvStartTime = (TextView) listItem.findViewById(R.id.start_time);
            TextView tvEndTime = (TextView) listItem.findViewById(R.id.end_time);
            TextView tvUser = (TextView) listItem.findViewById(R.id.user_tv);
            String letter = "A";

            letter = String.valueOf(gameType.get(pos).charAt(0)).toUpperCase();


            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRoundRect(letter, color, 20);



            //  iv.setBackgroundResource(thumb[pos]);  // **************
            try {
                tvGameType.setText(gameType.get(pos));
                System.out.println("gameType " + gameType.get(pos));

                tvDate.setText(date.get(pos));
                System.out.println("date " + date.get(pos));

                tvStartTime.setText(startTime.get(pos));
                System.out.println("start " + startTime.get(pos));

                tvEndTime.setText(endTime.get(pos));
                System.out.println("end " + endTime.get(pos));

                String newUser = (user.get(pos)).replace('_', '.');
                tvUser.setText(newUser);
                System.out.println("user " + newUser);

                iv.setImageDrawable(drawable);
            }
            catch (Exception e)
            {
                recreate();
                System.out.println("Exc: " + e);
                Toast.makeText(getApplicationContext(), getString(R.string.handled_unexpected), Toast.LENGTH_LONG).show();
            }

            return listItem;
        }

    }

    }




