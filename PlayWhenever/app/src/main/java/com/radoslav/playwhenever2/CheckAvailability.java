package com.radoslav.playwhenever2;

import java.util.ArrayList;

public class CheckAvailability {
    Boolean result = false;

    public CheckAvailability (ArrayList<Float> ArrList, Float TimeStart, Float TimeEnd) {

        try {
            System.out.println("TimeStart " + TimeStart + "\n");
            System.out.println("TimeStarArrList " + ArrList.get(0) + "\n");

            System.out.println("TimeEnd " + TimeEnd + "\n");
            System.out.println("TimeEndArrList " + ArrList.get(1) + "\n");

            if (TimeStart >= ArrList.get(0) && TimeEnd <= ArrList.get(1)) {
                result = true;
            } else {
                for (int i = 2; i < ArrList.size(); i += 2) {
                    if (TimeStart >= ArrList.get(i) && TimeEnd <= ArrList.get(i + 1)) {
                        result = true;
                    }
                }
            }

        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }
    public Boolean getResult()
    {
        return result;
    }

}