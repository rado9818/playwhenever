package com.radoslav.playwhenever2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

public class AddPlayground extends AppCompatActivity {

    private EditText name, location;
    private Spinner locale;
    private Button add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_playground);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        name = (EditText) findViewById(R.id.name);
        location = (EditText) findViewById(R.id.location);
        locale = (Spinner) findViewById(R.id.localesSpinner);
        add = (Button) findViewById(R.id.addButton);



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name.setError(null);
                location.setError(null);

                if(nameNotTooLong()) {
                    if(locationNotTooLong()) {
                        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds");
                        Map<String, Object> playgrouds = new HashMap<>();
                        String playgroundName = name.getText().toString();
                        playgrouds.put(playgroundName, "name");
                        ref.updateChildren(playgrouds);


                        Firebase newRef = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playgroundName);
                        Map<String, Object> attributes = new HashMap<>();
                        attributes.put("location", location.getText().toString());
                        attributes.put("locale", locale.getSelectedItem().toString());
                        newRef.updateChildren(attributes);
                        Toast.makeText(getApplicationContext(), R.string.added, Toast.LENGTH_LONG).show();

                    }
                    else
                    {
                        location.setError(getString(R.string.location_text_error));
                    }
                }
                else {
                    name.setError(getString(R.string.name_too_long));
                }

            }
        });


    }



    private boolean nameNotTooLong()
    {
        String tags = name.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<30 && chars>5;
    }

    private boolean locationNotTooLong()
    {
        String tags = location.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<30 && chars>5;
    }


}
