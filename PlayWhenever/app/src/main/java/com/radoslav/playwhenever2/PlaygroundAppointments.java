package com.radoslav.playwhenever2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PlaygroundAppointments extends AppCompatActivity implements OnShowcaseEventListener {
    FloatingActionButton fab;
    private ArrayList<String> usersWhoAdded = new ArrayList<>();
    private ArrayList<String> dates = new ArrayList<>();
    private ArrayList<String> startTimes = new ArrayList<>();
    private ArrayList<String> endTimes = new ArrayList<>();
    private String playground;
    private ListView lv;
    private String locale;
    private Button add;
    private ArrayList<Float> hoursBusy = new ArrayList<>();
    TextView overrideText;

    //for the showcase
    final String PREFS_NAME = "No appointments yet!";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;
    ShowcaseView sv;

    TextView noAppointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playground_appointments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        playground = intent.getStringExtra("playground");
        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground);
        lv = (ListView) findViewById(R.id.listView);
        overrideText = (TextView) findViewById(R.id.overrideText);
        noAppointments = (TextView) findViewById(R.id.no_appointments_txt);

        add = (Button) findViewById(R.id.addAppointment);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaygroundAppointments.this, AddAppointment.class);
                intent.putExtra("playground", playground);
                intent.putExtra("locale", locale);

                Float [] hoursBusyArray = new Float[hoursBusy.size()];
                hoursBusy.toArray(hoursBusyArray);

                for (int i = 0; i<hoursBusyArray.length; i++)
                {
                    System.out.print("before UserDetails " + hoursBusyArray[i]);
                }

                UserDetails.hours = hoursBusyArray;
                startActivity(intent);
            }
        });

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                usersWhoAdded.clear();
                dates.clear();
                endTimes.clear();
                startTimes.clear();

                hoursBusy.clear();

                if(dataSnapshot.hasChild(UserDetails.userName))
                {
                    overrideText.setVisibility(View.VISIBLE);
                }
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    System.out.println(child.getKey());

                    if(!child.getKey().equals("location") && (!child.getKey().equals("locale")))
                    {
                        usersWhoAdded.add(child.getKey());
System.out.print(child.getKey());
                        GetPlaygroundCharacteristics get = child.getValue(GetPlaygroundCharacteristics.class);

                        dates.add(get.getDate());
                        endTimes.add(get.getEndTime());
                        startTimes.add(get.getStartTime());

                        hoursBusy.add(timeToFloat(get.getStartTime()));
                        hoursBusy.add(timeToFloat(get.getEndTime()));
                        final Calendar c = Calendar.getInstance();

                    /*    System.out.println("hours: " +  c.get(Calendar.HOUR) + "\n");
                        System.out.println("date: " + c.get(Calendar.DAY_OF_MONTH) + 1 + "/" + c.get(Calendar.MONTH) + "\n");
                        System.out.println("year: " + c.get(Calendar.YEAR) );*/
                        Date date = Calendar.getInstance().getTime();

                        SimpleDateFormat mdyFormat = new SimpleDateFormat("dd/M/yyyy");

                     //   String date = c.get(Calendar.DAY_OF_MONTH) + "-" + c.get(Calendar.MONTH) + 1 + "-" + c.get(Calendar.YEAR);
                       String finalDate = mdyFormat.format(date);


                        Float taskTime = timeToFloat(get.getStartTime());
                        Float deviceTime = timeToFloat(c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));
                        String[] fromDb = get.getDate().split("/");

                        System.out.print("c.get(Calendar.DAY_OF_MONTH) " + c.get(Calendar.DAY_OF_MONTH) + "\n");
                        System.out.print("Integer.parseInt(fromDb[0]) " + Integer.parseInt(fromDb[0]) + "\n");

                        int month = c.get(Calendar.MONTH)+1;

                        System.out.print("c.get(Calendar.MONTH) " + month + "\n");
                        System.out.print("Integer.parseInt(fromDb[1]) " + Integer.parseInt(fromDb[1]) + "\n");

                        System.out.print("c.get(Calendar.YEAR) " + c.get(Calendar.YEAR) + "\n");
                        System.out.print("Integer.parseInt(fromDb[2]) " + Integer.parseInt(fromDb[2]) + "\n");

                        System.out.print("taskTime " + taskTime + "\n");
                        System.out.print("deviceTime " + deviceTime + "\n");

                        if((month > Integer.parseInt(fromDb[1])) || (c.get(Calendar.YEAR) > Integer.parseInt(fromDb[2])))
                        {
                            removeElement(child.getKey());
                            System.out.println(child.getKey() + " has already played");
                        }

                        else if((   c.get(Calendar.DAY_OF_MONTH)>Integer.parseInt(fromDb[0])   &&     month >= Integer.parseInt(fromDb[1])    &&     c.get(Calendar.YEAR) >= Integer.parseInt(fromDb[2])     ))
                        {

                           removeElement(child.getKey());
                            System.out.println(child.getKey() + " has already played");
                        }
                        else if ((   c.get(Calendar.DAY_OF_MONTH)==Integer.parseInt(fromDb[0])   &&     month == Integer.parseInt(fromDb[1])    &&     c.get(Calendar.YEAR) == Integer.parseInt(fromDb[2])     ) && (taskTime < deviceTime))
                        {
                            removeElement(child.getKey());
                        }

                        System.out.print("date: " + get.getDate() + "\n");
                        System.out.print("end: " + get.getEndTime() + "\n");
                        System.out.print("start: " + get.getStartTime() + "\n");

                    }
                    else
                    {

                        if(child.getKey().equals("locale"))
                        {
                            System.out.println("cool " + child.getValue()+"");
                            locale = child.getValue()+"";
                        }
                    }
                }


                lv.setAdapter(new VersionAdapter(PlaygroundAppointments.this));
                lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


                int currentVersionCode = 0;
                try {
                    currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    return;
                }
                SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);
                if(dates.size()==0)
                {
                    noAppointments.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    int margin = ((Number) (getResources().getDisplayMetrics().density * 36)).intValue();
                    lps.setMargins(margin, margin, margin, margin);

                    ViewTarget target = new ViewTarget(R.id.addAppointment, PlaygroundAppointments.this);
                    sv = new ShowcaseView.Builder(PlaygroundAppointments.this)
                            .withMaterialShowcase()
                            .setTarget(target)
                            .setContentTitle(getString(R.string.no_appointment))
                            .setContentText(getString(R.string.be_the_first_to_appoint))
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .setShowcaseEventListener(PlaygroundAppointments.this)
                            .build();
                    sv.setButtonPosition(lps);


                prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }
    @Override
    protected void onRestart() {
        super.onRestart();
        recreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_playground_appointments, menu);
        return true;
    }

    /**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.report_menu:
                Intent intent = new Intent(this, ReportSamePlayground.class);
                intent.putExtra("playground", playground);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void removeElement(String child)
    {
        Firebase ref = new Firebase(Constants.firebaseUrl).child("playgrounds/" + playground + "/" + child);
        ref.removeValue();
        System.out.println(child + " has already played");
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(PlaygroundAppointments activity) {
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return usersWhoAdded.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item_appointments, null);
            }

            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView name = (TextView) listItem.findViewById(R.id.name);
            TextView dateTv = (TextView) listItem.findViewById(R.id.dateText);
            TextView start = (TextView) listItem.findViewById(R.id.start);
            TextView end = (TextView) listItem.findViewById(R.id.end);
            String letter = "A";

            letter = String.valueOf(usersWhoAdded.get(pos).charAt(0)).toUpperCase();


            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRoundRect(letter, color, 20);



            name.setText(usersWhoAdded.get(pos));
            dateTv.setText(dates.get(pos));
            start.setText(startTimes.get(pos));
            end.setText(endTimes.get(pos));
            iv.setImageDrawable(drawable);

            return listItem;
        }

    }


    private Float timeToFloat(String time)
    {
        float timeFloat = Float.parseFloat(time.replace(":", "."));
        float resultTo100 = convertTo100(timeFloat);
        System.out.println(resultTo100);
        return resultTo100;
    }


    private static float convertTo100(float input)
    {
        String input_string = Float.toString(input);
        BigDecimal inputBD = new BigDecimal(input_string);
        String hhStr = input_string.split("\\.")[0];
        BigDecimal output = new BigDecimal(Float.toString(Integer.parseInt(hhStr)));
        output = output.add((inputBD.subtract(output).divide(BigDecimal.valueOf(60), 10, BigDecimal.ROUND_HALF_EVEN)).multiply(BigDecimal.valueOf(100)));

        return Float.parseFloat(output.toString());
    }

}
