package com.radoslav.playwhenever2;

public class GetPlaygroundCharacteristics {
    private String date;
    private String startTime;
    private String endTime;

    public GetPlaygroundCharacteristics() {
        // empty default constructor, necessary for Firebase to be able to deserialize blog posts
    }
    public String getDate() {
        return date;
    }
    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}